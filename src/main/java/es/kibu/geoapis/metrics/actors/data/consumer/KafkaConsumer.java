package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.japi.function.Function;
import akka.kafka.ConsumerSettings;
import akka.kafka.ProducerSettings;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.kafka.javadsl.Producer;
import akka.stream.*;
import akka.stream.javadsl.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import es.kibu.geoapis.objectmodel.dimensions.Constants;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;

import static es.kibu.geoapis.metrics.actors.KafkaConsts.DATA_CHANGE;
import static es.kibu.geoapis.metrics.actors.KafkaConsts.METRICS_DATA;

/**
 * Created by lrodriguez2002cu on 21/12/2016.
 */

public class KafkaConsumer {

    public static final String DATA_CHANGE_KEY = "data_change";

    static Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    public KafkaConsumer() {

    }

    //region How to use rest kafka services
/*   # Produce a message using JSON with the value '{ "foo": "bar" }' to the topic test
    $ curl -X POST -H "Content-Type: application/vnd.kafka.json.v1+json" \
            --data '{"records":[{"value":{"foo":"bar"}}]}' "http://localhost:8082/topics/jsontest"
    {"offsets":[{"partition":0,"offset":0,"error_code":null,"error":null}],"key_schema_id":null,"value_schema_id":null}

    # Create a consumer for JSON data, starting at the beginning of the topic's
    # log. Then consume some data from a topic using the base URL in the first response.

    # Finally, close the consumer with a DELETE to make it leave the group and clean up
    # its resources.
    $ curl -X POST -H "Content-Type: application/vnd.kafka.v1+json" \
            --data '{"name": "my_consumer_instance", "format": "json", "auto.offset.reset": "smallest"}' \
    http://localhost:8082/consumers/my_json_consumer
    {"instance_id":"my_consumer_instance",
            "base_uri":"http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance"}
    $ curl -X GET -H "Accept: application/vnd.kafka.json.v1+json" \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance/topics/jsontest
            [{"key":null,"value":{"foo":"bar"},"partition":0,"offset":0}]
    $ curl -X DELETE \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance

    */
    //endregion

    public void start(ActorSystem system, String configResourceBaseName, Sink<WriteInstruction, NotUsed> sink) {

        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, configResourceBaseName);


        final Function<Throwable, Supervision.Directive> decider = exc -> {
            //if (exc instanceof ArithmeticException)
            return Supervision.resume();
                /*else
                    return Supervision.stop();*/
        };

        //Function<Throwable, Supervision.Directive> decider1 = decider;
        final Materializer materializer = ActorMaterializer.create(ActorMaterializerSettings.create(system).withSupervisionStrategy(decider), system);

        //Consumer is represented by actor
        //ActorRef consumer = system.actorOf((KafkaConsumerActor.props(consumerSettings)));

        logger.debug("Creating the sink ...");

        final Sink<WriteInstruction, NotUsed> writeVariableSink = sink;

        //Manually assign topic partition to it
        logger.debug("Creating consumer ...");

        Source<ConsumerRecord<String, String>, ?> source =
                (Source<ConsumerRecord<String, String>, ?>) Consumer.plainSource(consumerSettings,
                        Subscriptions.topics(METRICS_DATA));

        ProducerSettings<String, String> producerSettings = getProducerSettings(system, configResourceBaseName);


        logger.debug("creating the graph...");
        broadcastGraph(DATA_CHANGE, source, writeVariableSink, producerSettings, materializer);

    }

    private WriteInstruction getWriteInstruction(ConsumerRecord/*<Object, Object>*/ t) {
        String key = (String) t.key();
        KeyDetails keyDetails = KafkaConsumer.decodeKey(key);
        String application = keyDetails.application;
        String variable = keyDetails.variable;

        logger.debug("getting variable from {} record key:{} value:{}", t.topic(), key, t.value());
        logger.debug("Variables app:{} variable:{}", application, variable);

        WriteInstruction writeInstruction = new WriteInstruction(variable, t.value().toString(), getWritingContext(getData(t), variable, "")
                /*new WriteInstruction.WriteContext("fail","fail","fail","fail","fail" )*/
        );

        logger.debug("Instruction: {}", writeInstruction);
        return writeInstruction;
    }


    private void broadcastGraph(final String topic, Source<ConsumerRecord<String, String>, ?> source, Sink<WriteInstruction, NotUsed> writeVariableSink,
                                ProducerSettings<String, String> producerSettings, Materializer mat) {

        RunnableGraph.fromGraph(GraphDSL.create(b -> {

            //prepare the broadcast to two different sinks
            final UniformFanOutShape<ConsumerRecord<String, String>, ConsumerRecord<String, String>/*WriteInstruction*/> bcast = b.add(Broadcast.create(2));

            //from consumer source to WriteInstruction
            final FlowShape<ConsumerRecord, WriteInstruction> toWriteInstruction =
                    b.add(Flow.of(ConsumerRecord.class).map(this::getWriteInstruction));

            final FlowShape<ConsumerRecord, WriteInstruction> toWriteInstruction1 =
                    b.add(Flow.of(ConsumerRecord.class).map(this::getWriteInstruction));

            //from WriteInstruction to ProducerRecord
            final FlowShape<WriteInstruction, ProducerRecord> toProducerRecord =
                    b.add(Flow.of(WriteInstruction.class).map(t -> new ProducerRecord(topic, DATA_CHANGE_KEY, new Gson().toJson(t))));

            //This are the sinks, the producer sink and the write Sink
            final SinkShape producerSink = b.add(Producer.<String, String>plainSink(producerSettings));
            final SinkShape<WriteInstruction> writeSink = b.add(writeVariableSink);

            b.from(b.add(source))
                    .viaFanOut(bcast)
                    .via(toWriteInstruction)
                    .via(toProducerRecord)
                    .to(producerSink);
            //via.to(writeSink);
            b.from(bcast)
                    .via(toWriteInstruction1)
                    .to(writeSink);

            return ClosedShape.getInstance();
        })).run(mat);
    }

    public static ProducerSettings<String, String> getProducerSettings(ActorSystem system, String configResourceBaseName) {
        Config config = ConfigFactory.load(configResourceBaseName).getConfig("akka.kafka.producer");
        String bootstrapServers = config.getString("bootstrap.servers");
        final ProducerSettings<String, String> producerSettings = ProducerSettings
                .create(system, new StringSerializer(), new StringSerializer())
                .withBootstrapServers(bootstrapServers);

        return producerSettings;
    }

    static ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, String configResourceBaseName) {
        Config config = ConfigFactory.load(configResourceBaseName).getConfig("akka.kafka.consumer");
        String bootstrapServers = config.getString("bootstrap.servers");
        if (configResourceBaseName != null && !configResourceBaseName.isEmpty()) {
            logger.debug("Creating consumer settings from: {} ", configResourceBaseName);
            return ConsumerSettings.create(config/*system*/, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    .withGroupId(".")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        } else {
            logger.debug("Creating consumer settings from ActorSystem");
            return ConsumerSettings.create(system, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    .withGroupId(".")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        }
    }


    static WriteInstruction.WriteContext getWritingContext(String data, String variable, String metricId) {
        try {
            logger.debug("data: {}", data);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(data);
            if (jsonNode.has(Constants.APPLICATION) && jsonNode.has(Constants.USER) && jsonNode.has(Constants.SESSION)) {
                String application = jsonNode.get(Constants.APPLICATION).asText();
                String user = jsonNode.get(Constants.USER).asText();
                String session = jsonNode.get(Constants.SESSION).asText();
                return new WriteInstruction.WriteContext(application, user, session, variable, metricId);
            }
        } catch (IOException e) {
            logger.error("Error while building the WriteInstruction context", e);
        }
        return WriteInstruction.WriteContext.failed();
    }


    private String getData(ConsumerRecord t) {
        return t.value().toString();
    }


    public static class KeyDetails implements Serializable {
        String variable;
        String application;

        public KeyDetails(String variable, String application) {
            this.variable = variable;
            this.application = application;
        }
    }

    public static KeyDetails decodeKey(String key) {
        logger.debug("Key to decode: {}", key);
        if (key.startsWith("\"") && key.endsWith("\"")) {
            key = key.substring(1, key.length() - 1);
        }

        logger.debug("Cleaned key: {}", key);

        String[] split = key.split(":");
        String application = split[0];
        String variable = split[1];
        return new KeyDetails(variable, application);
    }

}
