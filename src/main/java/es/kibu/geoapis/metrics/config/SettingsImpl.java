/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.metrics.config;

import akka.actor.Extension;
import com.typesafe.config.Config;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 04/12/2015.
 */

/*

myapp {
        db {
        uri = "mongodb://example1.com:27017,example2.com:27017"
        }
        circuit-breaker {
        timeout = 30 seconds
        }
        }

*/
public class SettingsImpl implements Extension {

    public static final String DATA_ENGINE = "data-engine";
    public static final String GEOFENCES_BACKEND_TESTING_KEY = DATA_ENGINE + ".testing";

    //Cassandra connection
    public final String CASSANDRA_DB_URI;
    public final String CASSANDRA_DB_NAME;
    public final String CASSANDRA_DB_USER;
    public final String CASSANDRA_DB_PASS;
    public final int CASSANDRA_REPLICATION_FACTOR;
    public final boolean DRY_RUN;

  /*  public final String  PERSISTENCE_BACKEND;
    public final String  SPARK_MASTER;
  */

    public final Duration METRICS_DEFINITION_RETRIEVAL_TIMEOUT;

    private boolean isTesting(){
       return config.hasPath(GEOFENCES_BACKEND_TESTING_KEY) && config.getBoolean(GEOFENCES_BACKEND_TESTING_KEY);
    }

    private String getResource(String resource){
        String format = (isTesting())?"%s-testing":"%s";
        return String.format(format, resource);
    }

    Config config;

    public SettingsImpl(Config config) {
        this.config  = config;

        CASSANDRA_DB_URI = config.getString(DATA_ENGINE + ".cassandra.uri");
        CASSANDRA_DB_NAME =  config.getString(DATA_ENGINE + ".cassandra.keyspace");
        CASSANDRA_DB_USER =  config.getString(DATA_ENGINE + ".cassandra.user");
        CASSANDRA_DB_PASS =  config.getString(DATA_ENGINE + ".cassandra.password");
        String replicationFactorConfig = DATA_ENGINE + ".cassandra.replication-factor";

        CASSANDRA_REPLICATION_FACTOR = config.hasPath(replicationFactorConfig)?config.getInt(replicationFactorConfig): 1;

        DRY_RUN = config.getBoolean(DATA_ENGINE + ".dry-run");
        //PERSISTENCE_BACKEND =  config.getString("metrics-backend.persistence-backend");

        java.time.Duration duration = config.getDuration(DATA_ENGINE + ".metrics-definition-retrieval-timeout");
        METRICS_DEFINITION_RETRIEVAL_TIMEOUT = Duration.create(duration.toMillis(), TimeUnit.MILLISECONDS);

    }

}