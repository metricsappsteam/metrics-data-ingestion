package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.japi.pf.ReceiveBuilder;
import akka.kafka.ConsumerSettings;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.stream.*;
import akka.stream.javadsl.*;
import akka.testkit.JavaTestKit;
import akka.util.ByteString;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import es.kibu.geoapis.metrics.actors.KafkaConsts;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import kamon.Kamon;
import mockit.Expectations;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.apache.parquet.Log;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;
import scala.reflect.ClassTag;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static es.kibu.geoapis.metrics.actors.KafkaConsts.METRICS_DATA;
import static es.kibu.geoapis.metrics.actors.data.consumer.KafkaConsumer.getConsumerSettings;


/**
 * Created by lrodriguez2002cu on 21/12/2016.
 */

@RunWith(JMockit.class)
public class KafkaConsumerTest {

    /*@ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder()
            .file("src/test/resources/docker/kafka-docker-compose.yml")
            .waitingForService("kafka",HealthChecks.toHaveAllPortsOpen())
            .build();
    */

    public static final String TEST_CONFIG_FILE = "data-ingestion.test.conf";

    public static final String SAMPLE_METRICS_SAMPLE_METRIC_WIKILOC_BOX_SPEED_JSON = "sample-metrics/sample-metric-wikiloc-box-speed.json";

    public static final String RESTAPI_ENDPOINT = "http://%s:8082/topics/" + METRICS_DATA;
    public static final String AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS = "akka.kafka.consumer.bootstrap.servers";

    public static final String getRestEndPoint() {
        Config config = ConfigFactory.load(TEST_CONFIG_FILE);
        if (config.hasPath(AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS)) {
            String server = config.getString(AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS).split(":")[0];
            return String.format(RESTAPI_ENDPOINT, server);
        }
        throw new RuntimeException("Endpoint server not defined");
    }

    @Test
    public void test() throws ExecutionException, InterruptedException {
        Config config = ConfigFactory.load();
        ActorSystem system = ActorSystem.create("KafkaSystem", config);
        ActorMaterializer mat = ActorMaterializer.create(system);

        final Source<Integer, NotUsed> in = Source.from(Arrays.asList(1, 2, 3, 4, 5));

        final Sink<List<String>, CompletionStage<List<String>>> sink = Sink.head();
        final Flow<Integer, Integer, NotUsed> f1 = Flow.of(Integer.class).map(elem -> elem + 10);
        final Flow<Integer, Integer, NotUsed> f2 = Flow.of(Integer.class).map(elem -> elem + 20);
        final Flow<Integer, String, NotUsed> f3 = Flow.of(Integer.class).map(elem -> elem.toString());
        final Flow<Integer, Integer, NotUsed> f4 = Flow.of(Integer.class).map(elem -> elem + 30);

        Graph<ClosedShape, CompletionStage<List<String>>> graph = GraphDSL     // create() function binds sink, out which is sink's out port and builder DSL
                .create(   // we need to reference out's shape in the builder DSL below (in to() function)
                        sink,                // previously created sink (Sink)
                        (builder, out) -> {  // variables: builder (GraphDSL.Builder) and out (SinkShape)
                            final UniformFanOutShape<Integer, Integer> bcast = builder.add(Broadcast.create(2));
                            final UniformFanInShape<Integer, Integer> merge = builder.add(Merge.create(2));

                            final Outlet<Integer> source = builder.add(in).out();
                            builder.from(source).via(builder.add(f1))
                                    .viaFanOut(bcast).via(builder.add(f2)).viaFanIn(merge)
                                    .via(builder.add(f3.grouped(1000))).to(out);  // to() expects a SinkShape
                            builder.from(bcast).via(builder.add(f4)).toFanIn(merge);
                            return ClosedShape.getInstance();
                        });

        final RunnableGraph<CompletionStage<List<String>>> result =
                RunnableGraph.fromGraph(
                        graph);

        List<String> strings = result.run(mat).toCompletableFuture().get();
        System.out.println(strings);

        system.terminate();
    }


    /**
     * This request
     * POST /topics/metrics-data HTTP/1.1
     * Host: localhost:8082
     * Content-Type: application/vnd.kafka.json.v1+json
     * Cache-Control: no-cache
     * Postman-Token: 71e519f4-df5a-d250-8b02-02d633bb6e0a
     * <p>
     * {"records":[
     * {
     * "key": "app12345:movement2" ,
     * "value": {
     * "application": "app1",
     * "user": "user1",
     * "session": "session1"
     * }
     * }
     * ]
     * }
     */
    @Test
    @Ignore
    public void testConsumer() throws ExecutionException, InterruptedException {

        Config config = ConfigFactory.load();
        ActorSystem system = ActorSystem.create("KafkaConsumerTestSystem", config);

        KafkaConsumer consumer = new KafkaConsumer();
        consumer.start(system, "data-ingestion.conf", SinkFactory.cassandraSinkActor(system));

        system.awaitTermination(new FiniteDuration(1, TimeUnit.MINUTES));
    }


    void createDataChangeConsumerActor(ActorRef testActor) {
        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, TEST_CONFIG_FILE);
        //Consumer is represented by actor
        //ActorRef consumer = system.actorOf((KafkaConsumerActor.props(consumerSettings)));
        ActorMaterializer materializer = ActorMaterializer.create(system);
        Consumer.plainSource(consumerSettings, Subscriptions.topics(KafkaConsts.DATA_CHANGE))
                .map(t -> {
                    System.out.println("data change ... (" + t.key() + ", " + t.value() +")");
                    return "data_change";
                })
                .runWith(Sink.actorRef(testActor, "finished"), materializer);

    }


    Logger logger = LoggerFactory.getLogger(KafkaConsumerTest.class);
    private static ActorSystem system;

    @BeforeClass
    public static void setUp() throws Exception {
        Kamon.start();
        system = ActorSystem.create();
    }

    public static class PostProducer extends AbstractActor {

        Logger logger = LoggerFactory.getLogger(PostProducer.class);

        final Http http = Http.get(context().system());


        final Materializer materializer = ActorMaterializer.create(context());

        public static Props props() {
            ClassTag<PostProducer> tag = scala.reflect.ClassTag$.MODULE$.apply(PostProducer.class);
            return Props.apply(tag);
        }

        static class ProduceMessage implements Serializable {
            String url;
            String entity;

            //String header;
            public ProduceMessage(String url, String entity) {
                this.url = url;
                this.entity = entity;
            }

            @Override
            public String toString() {
                return entity;
            }
        }

        public PostProducer() {

            receive(ReceiveBuilder
                    .match(ProduceMessage.class, msg -> {
                        produce(msg).whenComplete((response, ex) -> {
                            logger.debug("Completed {}", response.status());
                        });
                    }).match(Object.class, msg1 -> {
                        logger.debug("Message of class {} logged", msg1);
                    }).build());
        }

        CompletionStage<HttpResponse> produce(ProduceMessage msg) {
            MediaType.Binary mediaType = MediaTypes.applicationBinary("vnd.kafka.json.v1+json", true, "json");
            ContentType binary = ContentTypes.create(mediaType);
            HttpEntity.Strict entity = HttpEntities.create(binary, ByteString.fromString(msg.entity));
            //logger.debug("Sending post request to: {}, entity: {}", msg.url, msg.entity);
            HttpRequest post = HttpRequest.POST(msg.url).withEntity(entity);
            logger.info("Sending message {}", post);
            return http.singleRequest(post, materializer).toCompletableFuture();
        }
    }

    public static final String testBodyTemplate =
            "{\"records\":[\n" +
                    "\t{\n" +
                    "\t\"key\": \"%s\" ,\n" +
                    "\t\"value\": {\n" +
                    "\t\t\"application\": \"%s\",\n" +
                    "\t\t\"user\": \"%s\",\n" +
                    "\t\t\"session\": \"%s\"\n" +
                    "\t\t}\n" +
                    "\t}\n" +
                    "]\n" +
                    "\t\n" +
                    "}";

    public PostProducer.ProduceMessage getSample(String application, String variable, String usr, String session) {
        String body = String.format(testBodyTemplate, application + ":" + variable, application, usr, session);
        logger.debug("Rendered body: {}", body);
        return new PostProducer.ProduceMessage(getRestEndPoint(), body);
    }

    public PostProducer.ProduceMessage getSample(String application, String variable) {
        return getSample(application, variable, "usr1", "session1");
    }


    private String getRandomVariable() {
        String variable = "variable";
        return getRandomString(variable);
    }

    private String getRandomApplication() {
        String variable = "application";
        return getRandomString(variable);
    }

    private String getRandomString(String prefix) {
        return prefix + org.joda.time.DateTime.now().getMillis();
    }


    private static class Counter {
        int dataChangeCount = 0;
        int dataOkCount = 0;

        void updateCounter(String okMsg) {
            dataChangeCount += okMsg.contains("data_change") ? 1 : 0;
            dataOkCount += okMsg.contains("ok") ? 1 : 0;
        }
    }

    @Test
    public void testConsumerActors(/*@Mocked TestHandler handler*/) throws IOException, ProcessingException {

        InputStream resourceAsStream = KafkaConsumerTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        //String metricString = IOUtils.toString(resourceAsStream, "UTF-8");
        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{
            //this should be called twice
/*
            manager.getMetricsDef((String) any);  times = 2;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)));
*/
        }};

        final Counter counter = new Counter();
        final int WRITE_INST_COUNT = 10; //Math.round(Math.abs(Math.random()));

        new JavaTestKit(system) {
            {
                ActorRef testActorRef = getTestActor();
                createDataChangeConsumerActor(testActorRef);

                KafkaConsumer consumer = new KafkaConsumer();
                ActorRef producerActorRef = system.actorOf(PostProducer.props());


                consumer.start(system, TEST_CONFIG_FILE, SinkFactory.actorSink(testActorRef));

                // can also use JavaTestKit “from the outside”
                final JavaTestKit probe = new JavaTestKit(system);

                // the run() method needs to finish within 3 seconds
                //MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

                final FiniteDuration duration = duration("20 seconds");

                String variable = getRandomVariable();
                String application = getRandomApplication();


                new Within(duration) {
                    protected void run() {

                        logger.debug("Expected and variable application: {}, variable: {}", application, variable);
                        for (int i= 0; i< WRITE_INST_COUNT; i++) {
                            producerActorRef.tell(getSample(application, variable), getRef());
                        }

                        int times = 0;
                        while (true) {
                            Object t = expectMsgAnyClassOf(WriteInstruction.class, String.class);
                            if (t instanceof WriteInstruction) {
                                WriteInstruction writeInstruction = (WriteInstruction) t;
                                logger.debug("Got a write instruction: {}", writeInstruction);
                                if (writeInstruction.getVariable().equalsIgnoreCase(variable) &&
                                        writeInstruction.getContext().getApplication().equalsIgnoreCase(application)) {
                                    getTestActor().tell("ok", ActorRef.noSender());
                                    //break;
                                    times ++;
                                    if (times == WRITE_INST_COUNT ) break;
                                }
                            }

                            if (t instanceof String) {
                                counter.updateCounter((String) t);
                            }

                        }
                    }
                };


                String okMsg = expectMsgClass(String.class);
                counter.updateCounter(okMsg);

                okMsg = expectMsgClass(String.class);

                counter.updateCounter(okMsg);


                logger.debug(String.format("counter.dataChangeCount: %d", counter.dataChangeCount));
                logger.debug(String.format("counter.dataOkCount: %d", counter.dataOkCount));

                //assertions of ok, or data_change
                Assert.assertTrue(counter.dataChangeCount > 0);
                Assert.assertTrue(counter.dataOkCount == WRITE_INST_COUNT);

            }
        };

        new Verifications() {{

        }};
    }

/*

public void testCacheActorInvalidationWorks() throws IOException, ProcessingException {

        InputStream resourceAsStream = MetricsDefinitionCacheActorTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        String metricString = IOUtils.toString(resourceAsStream, "UTF-8");
        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{

            //this should be called twice
            manager.getMetricsDef((String) any);  times = 2;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)));
        }};

        final String appid = "mocked-app";
        final String metricid = "mocked-metricid";

        new JavaTestKit(system) {{

            final Props props = MetricsDefinitionCacheActor.props();
            final ActorRef subject = system.actorOf(props);

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds

            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("40 seconds");

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == appid);
                    Assert.assertTrue(replyMessage.metricRef == metricid);
                }
            };

            //this new message does not cause the retrieval from store.
            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == appid);
                    Assert.assertTrue(replyMessage.metricRef == metricid);
                }
            };



            String unknown_appid = appid;//"unknown_appid";
            String unknown_metricid = metricid;//"unknown_metricid";

            //send an invalidate message
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msgInvalidate =
                    MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage(unknown_appid, unknown_metricid);
            subject.tell(msgInvalidate, getRef());

            //lets request it again!!. This should cause the retrieval of the deinition from storage again
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg1 = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(unknown_appid, unknown_metricid, MetricsDefinitionCacheActor.Action.GET);

            new Within(duration) {
                protected void run() {
                    subject.tell(msg1, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == unknown_appid);
                    Assert.assertTrue(replyMessage.metricRef == unknown_metricid);

                    logger.debug("Replied: {}", replyMessage);
                    Assert.assertTrue(!replyMessage.isError());

                }
            };

        }};

        new Verifications() {{

        }};
    }

*/
}